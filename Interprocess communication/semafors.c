#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>
#include <signal.h>

enum { BASE = 10 };

volatile int cnt, lives, n, semid;

void
handler (int sig)
{
    signal(SIGUSR1, handler);
    cnt--;
    struct sembuf parent = {n, 1, 0};
    semop(semid, &parent, 1);
}

int
main(int argc, char **argv)
{
    signal(SIGUSR1, handler);
    
    char *eptr = NULL;
    errno = 0;
    n = strtol(argv[1], &eptr, BASE);
    
    cnt = n;
            
    if (errno || *eptr || eptr == argv[1] || (int) n != n) {
        return 1;
    }
    
    lives = strtol(argv[2], &eptr, BASE);
            
    if (errno || *eptr || eptr == argv[2] || (int)lives != lives) {
        return 1;
    }
    
    semid = semget(IPC_PRIVATE, n + 1, IPC_CREAT | 0666);
    
    
    for (int i = 0; i < n + 1; i++) {
        semctl(semid, i, SETVAL, 0);
    }
    
    setbuf(stdin, NULL);
    
    for (int i = 0; i < n; i++) {
        if (fork() == 0) {
            while(1) {
                struct sembuf parent = {n, 1, 0};
                struct sembuf buf_cure = {i, -1, 0};
                semop(semid, &buf_cure, 1);
                
                while (semop(semid, &buf_cure, 1) >= 0) {
                    lives--;
                    printf("%d %d\n", i, lives);
                    fflush(stdout);
                    
                    if (lives == 0) {
                        pid_t parent_pid = getppid();
                        kill(parent_pid, SIGUSR1);
                        break;
                    }
                    
                    semop(semid, &parent, 1);
                }
                
                while (semop(semid, &buf_cure, 1) >= 0) {
                    semop(semid, &parent, 1);
                }
                
                _exit(0);
            }
            
            _exit(0);
        }
    }
    
    unsigned int cure;
    
    while (1) {

        if (scanf("%u", &cure) == EOF) {
            semctl(semid, IPC_RMID, 0);
            printf("%d\n", cnt);
            _exit(0);
        }
        
        cure = cure % n;
        struct sembuf buf_cure_1 = {cure, 1, 0};
        semop(semid, &buf_cure_1, 1);
    }

    semctl(semid, IPC_RMID, 0);
    
    return 0;
}
