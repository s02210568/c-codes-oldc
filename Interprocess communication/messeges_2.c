#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>
#include <signal.h>
#include <sys/msg.h>

enum { BASE = 10, SIZE = 16 };

struct Message
{
    long type;
    long long val_1;
    long long val_2;
};

int
main(int argc, char **argv)
{
    struct Message Message;
    
    char *eptr = NULL;
    errno = 0;
    long n = strtol(argv[2], &eptr, BASE);
            
    if (errno || *eptr || eptr == argv[2] || (int) n != n) {
        return 1;
    }
    
    key_t key = strtol(argv[1], &eptr, BASE);
    int msgid = msgget(key, IPC_CREAT | 0666);
    
    long long val_1 = strtoll(argv[3], &eptr, BASE);
    long long val_2 = strtoll(argv[4], &eptr, BASE);
    long long max = strtoll(argv[5], &eptr, BASE);
    
    pid_t pid;
    pid_t *pids = calloc(n, sizeof(*pids));
    
    if (pids == NULL) {
        _exit(1);
    }
    
    for (int i = 0; i < n; i++) {
        if ((pid = fork()) == 0) {
            while(1) {
                struct Message recieved;
                
                if (msgrcv(msgid, &recieved, SIZE, i + 1, 0) < 0) {
                    break;
                }
                
                long long x_1 = recieved.val_1;
                long long x_2 = recieved.val_2;
                long long x_3 = x_1 + x_2;
                
                printf("%d %lld\n", i, x_3);
                fflush(stdout);
                
                if (x_3 > max) {
                    msgctl(msgid, IPC_RMID, 0);
                    break;
                }
                
                Message.val_1 = x_2;
                Message.val_2 = x_3;
                Message.type = x_3 % n + 1;
                msgsnd(msgid, &Message, SIZE, 0);
            }
            
            _exit(0);
        } else if (pid < 0) {
            msgctl(msgid, IPC_RMID, 0);
            
            while (wait(NULL) != -1) {
            }
            
            _exit(1);
        }
    }
    
    Message.type = 1;
    Message.val_1 = val_1;
    Message.val_2 = val_2;
    msgsnd(msgid, &Message, SIZE, 0);
    
    while (wait(NULL) != -1) {
    }
    
    msgctl(msgid, IPC_RMID, 0);
    
    return 0;
}
