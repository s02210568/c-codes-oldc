#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>

pid_t pid_1, pid_2;
unsigned char byte = 0, cure_byte = 0;
int cure_len = 0, len = 0, fd, first = 0;

enum { BYTE_SIZE = 8 };

void
handler_main(int sig)
{
    signal(SIGALRM, handler_main);
}

void
handler_2(int sig)
{
    signal(SIGALRM, handler_2);
    
    while (len >= 0) {
        int bit = (cure_byte >> len) & 1;
            
        len--;
            
        if (bit) {
            kill(pid_1, SIGUSR2);
                
            return;
        } else {
            kill(pid_1, SIGUSR1);
                
            return;
        }
    }
    
    if (read(fd, &cure_byte, sizeof(cure_byte)) != sizeof(cure_byte)) {
        kill(pid_1, SIGIO);
        _exit(0);
    }
    
    len = BYTE_SIZE - 1;
    raise(SIGALRM);
}

void
handler_1(int sig)
{
    signal(SIGUSR1, handler_1);
    signal(SIGUSR2, handler_1);
    signal(SIGIO, handler_1);
    
    if (sig == SIGIO) {
        printf("%c", byte);
        fflush(stdout);
        
        _exit(0);
    }
    
    if (cure_len == BYTE_SIZE) {
        printf("%c", byte);
        fflush(stdout);
        
        byte = 0;
        cure_len = 0;
    }
    
    if (sig == SIGUSR2) {
        byte = (byte << 1) | 1;
        cure_len++;
    } else if (sig == SIGUSR1) {
        byte <<= 1;
        cure_len++;
    }
    
    kill(0, SIGALRM);
}

int
main(int argc, char** argv)
{
    signal(SIGALRM, handler_main);
    
    fd = open(argv[1], O_RDONLY);
    
    if (fd < 0) {
        return 1;
    }
    
    if ((pid_1 = fork()) == 0) { //первый получает сигнал SIGUSR2 - 1 SIGUSR1 - 0
        signal(SIGUSR1, handler_1);
        signal(SIGUSR2, handler_1);
        signal(SIGIO, handler_1);
        
        while (1) {
        }
    }
    
    if ((pid_2 = fork()) == 0) {
        signal(SIGALRM, handler_2);
        
        if (read(fd, &cure_byte, sizeof(cure_byte)) != sizeof(cure_byte)) {
            kill(pid_1, SIGIO);
            _exit(0);
        }
        
        len = BYTE_SIZE - 1;
        
        int bit = (cure_byte >> len) & 1;
        
        len--;
            
        if (bit) {
            kill(pid_1, SIGUSR2);
        } else {
            kill(pid_1, SIGUSR1);
        }
        
        while (1) {
        }
    }
    
    while (wait(NULL) != -1) {
    }
    
    if (close(fd) != 0) {
        return 1;
    }
    
    return 0;
}
