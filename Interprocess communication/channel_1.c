#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <math.h>

//cmd1 | cmd2 | cmd3

int main(int argc, char** argv) {
    int fd_1[2];
    pipe(fd_1);
    
    if (fork() == 0) {
        close(fd_1[0]);
        dup2(fd_1[1], 1);
        close(fd_1[1]);
        
        execlp(argv[1], argv[1], NULL);
        _exit(1);
    }
    
    close(fd_1[1]);
    
    int fd_2[2];
    pipe(fd_2);
    
    if (fork() == 0) {
        close(fd_1[1]);
        dup2(fd_1[0], 0);
        close(fd_1[0]);
        dup2(fd_2[1], 1);
        close(fd_2[1]);
        close(fd_2[0]);
        
        execlp(argv[2], argv[2], NULL);
        _exit(1);
    }
    
    close(fd_1[0]);
    close(fd_2[1]);
    
    if (fork() == 0) {
        dup2(fd_2[0], 0);
        close(fd_2[0]);
        
        execlp(argv[3], argv[3], NULL);
        _exit(1);
    }
    
    
    close(fd_2[0]);
    
    while(wait(NULL) != -1) {
    }
    
    return 0;
}
