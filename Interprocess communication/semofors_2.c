#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>
#include <signal.h>

enum { BASE = 10 };

int mod(int a, int b) {
    if ((a % b) < 0) {
        return ((a % b) + b);
    }

    return (a % b);
}

int
main(int argc, char **argv)
{
    char *eptr = NULL;
    errno = 0;
    long n = strtol(argv[1], &eptr, BASE);
            
    if (errno || *eptr || eptr == argv[1] || (int) n != n) {
        return 1;
    }
    
    int semid = semget(IPC_PRIVATE, n, IPC_CREAT | 0666);
    
    
    for (int i = 0; i < n; i++) {
        semctl(semid, i, SETVAL, 0);
    }
    
    semctl(semid, 0, SETVAL, 1);
    setbuf(stdin, NULL);
    
    for (int i = 0; i < n; i++) {
        if (fork() == 0) {
            while(1) {
                struct sembuf buf_cure = {i, -1, 0};
                
                if (semop(semid, &buf_cure, 1) < 0) {
                    _exit(0);
                }
                
                int cure;
                
                if (scanf("%d", &cure) == EOF) {
                    semctl(semid, IPC_RMID, 0);
                    break;
                }
                
                printf("%d %d\n", i, cure);
                fflush(stdout);
                
                buf_cure.sem_num = mod(cure, n);
                buf_cure.sem_op = 1;
                buf_cure.sem_flg = 0;
                
                semop(semid, &buf_cure, 1);
            }
            
            _exit(0);
        }
    }
    
    while (wait(NULL) != -1) {
    }

    semctl(semid, IPC_RMID, 0);
    
    return 0;
}
