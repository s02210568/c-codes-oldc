#include <stdio.h>
#include <stdlib.h>

void matrix_multiply(long** matrix_1, long** matrix_2, long** matrix_3, int size_matrix, int mod) {
    for (int i = 0; i < size_matrix; i++) {
        for (int j = 0; j < size_matrix; j++) {
            for (int k = 0; k < size_matrix; k++) {
                matrix_1[i][j] += ((matrix_2[i][k] % mod) * (matrix_3[k][j] % mod)) % mod;
            }
        }
    }
}

void free_memory(long** matrix, int size_matrix) {
    for (int i = 0; i < size_matrix; i++) {
        free(matrix[i]);
    }

    free(matrix);
}

void matrix_copy(long** matrix_from, long** matrix_to, int size_matrix) {
    for (int i = 0; i < size_matrix; i++) {
        for (int j = 0; j < size_matrix; j++) {
            matrix_to[i][j] = matrix_from[i][j];
        }
    }
}

void matrix_pow(long** matrix, long** matrix_powered, int degree, int size_matrix, int mod) {
    if (degree == 0) {
        for (int i = 0; i < size_matrix; i++) {
            matrix_powered[i][i] = 1;
        }

        return;
    }
    else if (degree == 1) {
        matrix_copy(matrix, matrix_powered, size_matrix);

        return;
    }
    else if (!(degree & 1)) {
        matrix_pow(matrix, matrix_powered, degree / 2, size_matrix, mod);

        long** powered_cure = (long**)malloc(size_matrix * sizeof(long*));

        for (int i = 0; i < size_matrix; i++) {
            powered_cure[i] = (long*)calloc(size_matrix, sizeof(long));
        }

        matrix_multiply(powered_cure, matrix_powered, matrix_powered, size_matrix, mod);

        matrix_copy(powered_cure, matrix_powered, size_matrix);

        free_memory(powered_cure, size_matrix);

        return;
    }

    matrix_pow(matrix, matrix_powered, degree - 1, size_matrix, mod);

    long** powered_cure = (long**)malloc(size_matrix * sizeof(long*));

    for (int i = 0; i < size_matrix; i++) {
        powered_cure[i] = (long*)calloc(size_matrix, sizeof(long));
    }

    matrix_multiply(powered_cure, matrix_powered, matrix, size_matrix, mod);

    matrix_copy(powered_cure, matrix_powered, size_matrix);

    free_memory(powered_cure, size_matrix);
}

int main(void) {
    int k, n, mod;
    scanf("%d%d%d", &k, &n, &mod);

    long* fib_seq = (long*)malloc(k * sizeof(long));

    for (int i = 0; i < k; i++) {
        scanf("%ld", &fib_seq[i]);
    }

    long** matrix = (long**)malloc(k * sizeof(long*));

    for (int i = 0; i < k; i++) {
        matrix[i] = (long*)calloc(k, sizeof(long));
    }

    for (int i = 0; i < k; i++) {
        long cure;
        scanf("%ld", &cure);
        matrix[0][i] = cure % mod;
    }

    for (int i = 1; i < k; i++) {
        matrix[i][i - 1] = 1;
    }

    long** matrix_powered = (long**)malloc(k * sizeof(long));

    for (int i = 0; i < k; i++) {
        matrix_powered[i] = (long*)calloc(k, sizeof(long));
    }

    if ((n - k) < 0) {
        printf("%ld", fib_seq[n - 1] % mod);
    }
    else {
        matrix_pow(matrix, matrix_powered, n - k, k, mod);

        long answer = 0;

        for (int i = 0; i < k; i++) {
            answer += (matrix_powered[0][i] * fib_seq[k - i - 1]) % mod;
        }

        printf("%ld", answer % mod);
    }
    
    free_memory(matrix_powered, k);

    return 0;
}
