#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 100 + 1

struct Node {
    int value;
    char *s;
    char *subs;
    struct Node* next;
};

void print_list_file(struct Node* head, char filename[]) {
    FILE*f = fopen(filename, "w");

    while (head) {
        fprintf(f, "%s : %s", head->subs, head->s);
        head = head->next;
    }

    fclose(f);
}

struct Node* create_new_node(char* substr, int value, char* str) {
    struct Node* new_node = (struct Node*)calloc(1, sizeof(struct Node));

    new_node->value = value;

    new_node->s = (char*)calloc(BUF_SIZE, sizeof(char));
    stpcpy(new_node->s, str);

    new_node->subs = (char*)calloc(BUF_SIZE, sizeof(char));
    stpcpy(new_node->subs, substr);

    new_node->next = NULL;

    return new_node;
}

void remove_spaces(char* s) {
    char* t = s;
    int flag = 1;
    while (*s) {
        if (*s == '"') {
            *t = *s;
            s++;
            t++;

            while (*s != '"') {
                *t = *s;
                s++;
                t++;
            }

            *t = *s;
            s++;
            t++;
            flag = 0;
        } else if (*s != ' ') {
            *t = *s;
            t++;
        }

        if (flag) {
            s++;
        }

        flag = 1;
    }
    *t = '\0';
}

int char_to_int(char c) {
    return c - 48;
}

int is_digit(char c) {
    if ((c >= '0') && (c <= '9')) {
        return 1;
    }
    return 0;
}

int get_value(char* s, int number) {
    int n = 0;
    int len = strlen(s);
    for (int i = 0; i < len; ++i) {
        if (s[i] == ';') {
            n++;
        }

        if (n == number) {
            i++;

            int is_signed = 1;
            if (s[i] == '-') {
                is_signed = -1;
                i++;
            }
            int value = char_to_int(s[i]);

            i++;
            while (is_digit(s[i]) == 1) {
                value = value * 10 + char_to_int(s[i]);
                i++;
            }

            return value * is_signed;
        }
    }

    return 1e9 + 1;
}

char* get_substr(char* s, int number) {
    int n = 0;

    for (int i = 0; i < BUF_SIZE; ++i) {
        if (s[i] == ';') {
            n++;
        }

        if (n == number) {
            i++;
            while (s[i] != '"') {
                i++;
            }
            i++;

            char* subs = (char*)calloc(BUF_SIZE, sizeof(char));
            int j = 0;
            while (s[i] != '"') {
                subs[j] = s[i];
                j++;
                i++;
            }

            return subs;
        }
    }

}

void bubble_sort_value(struct Node* head, int len) {
    for (int i = 0; i < len - 1; ++i) {
        struct Node*cur = head;
        while (cur->next) {
            int v1 = cur->value;
            int v2 = cur->next->value;
            char *s1 = cur->s;
            char *s2 = cur->next->s;

            if (v1 > v2) {
                cur->next->value = v1;
                cur->value = v2;

                cur->next->s = s1;
                cur->s = s2;
            }
            cur = cur->next;
        }

    }
}

void bubble_sort_subs(struct Node* head, int len) {
    for (int i = 0; i < len - 1; ++i) {
        struct Node*cur = head;
        while (cur->next) {

            char* subs1 = cur->subs;
            char* subs2 = cur->next->subs;

            char *s1 = cur->s;
            char *s2 = cur->next->s;

            if (strcmp(subs1, subs2) > 0) {
                cur->next->subs = subs1;
                cur->subs = subs2;

                cur->next->s = s1;
                cur->s = s2;
            }
            cur = cur->next;
        }

    }
}


void delete (struct Node* head) {
    struct Node*next;
    while (head) {
        next = head->next;
        free(head->s);
        free(head->subs);

        free(head);
        head = next;
    }
}

int main(void) {
    char filename[] = "input.txt";
    FILE* f1 = fopen(filename, "r");

    int number;//номер столбца для сорттировки
    fscanf(f1, "%d\n", &number);

    char* s = (char*)calloc(BUF_SIZE, sizeof(char));
    fgets(s, BUF_SIZE, f1);
    remove_spaces(s);
    int value = get_value(s, number);


    struct Node* head;

    if (value != 1e9 + 1) {
        head = create_new_node("", value, s);
        struct Node* cur = head;

        int len = 1;

        while (fgets(s, BUF_SIZE, f1) != NULL) {
            remove_spaces(s);
            value = get_value(s, number);
            cur->next = create_new_node("", value, s);
            cur = cur->next;
            len++;
        }

        int last = strlen(cur->s) - 1;
        (cur->s)[last + 1] = '\n';
        (cur->s)[last + 2] = '\0';

        bubble_sort_value(head, len);

    } else {
        char *substr = get_substr(s, number);
        head = create_new_node(substr, 0, s);
        struct Node* cur = head;

        int len = 1;

        while (fgets(s, BUF_SIZE, f1) != NULL) {
            remove_spaces(s);
            substr = get_substr(s, number);
            cur->next = create_new_node(substr, 0, s);
            cur = cur->next;
            len++;
        }

        int last = strlen(cur->s) - 1;
        (cur->s)[last + 1] = '\n';
        (cur->s)[last + 2] = '\0';           //повтор,нужно или нет????  в функции удаления пробелов нужно..
        bubble_sort_subs(head, len);
    }

    fclose(f1); 


    print_list_file(head, "output.txt");

    delete(head);

    return 0;
}
