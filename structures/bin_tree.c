#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 2000
#define ALPHABET_LEN 26

typedef struct TreeNode {
    int value;
    
    struct TreeNode* branches[ALPHABET_LEN];
} TreeNode;

void add(char* str, TreeNode* root, int* count) {
    TreeNode* cure = root;
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        if (cure->branches[str[i] - 'a'] == NULL) {
            cure->branches[str[i] - 'a']  = (TreeNode*)malloc(sizeof(TreeNode));
            (*count)++;
        }
        cure = cure->branches[str[i] - 'a'];
    }
}

int main(void) {
    int count = 1;
    struct TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));
    
    char* str = (char*)malloc(MAX_LEN * sizeof(char));
    
    fgets(str, MAX_LEN, stdin);
    int len = strlen(str) - 1;
    
    for (int i = 0; i < len; i++) {
        char* str_prefix = (char*)calloc(MAX_LEN, sizeof(char));
        
        for (int j = i, k = 0; j < len ; j++, k++) {
            str_prefix[k] = str[j];
        }
        
        add(str_prefix, root, &count);
        
        free(str_prefix);
    }
    
    free(str);
    
    printf("%d", count);
    
    return 0;
}
