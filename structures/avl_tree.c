#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 101

typedef struct avlnode {
	long key;
	long data;
	struct avlnode* right;
	struct avlnode* left;
	int height;
} avlnode;

 void printing(avlnode* root) {
 	if (root) {
 		printf("%ld - ", root->key);
 		if (root->right) {
 			printf("%ld ", root->right->key);
 		} else {
 			printf("NULL ");
 		}
 		if (root->left) {
 			printf("%ld   ", root->left->key);
 		} else {
 			printf("NULL    ");
 		}
 		printf("height = %d\n", root->height);


 		printing(root->right);
 		printing(root->left);
 	}
 }

avlnode* delete(avlnode* root) {
	if (root) {
		delete(root->right);
		delete(root->left);
		free(root);
	}
	return NULL;
}

void find(avlnode* root, long key) {
	if (root == NULL) {
		return;
	}

	if (key > root->key) {
		find(root->right, key);
	} else if (key < root->key) {
		find(root->left, key);
	} else if (key == root->key) {
		printf("%ld %ld\n", root->key, root->data);
	}
}

static inline int max(int a, int b) {
	return a > b ? a : b;
}
static inline int height(avlnode* p) {
	return p ? p->height : 0;
}

// static avlnode* SingleRotateWithRight(avlnode* father);
// static avlnode* SingleRotateWithLeft(avlnode* father);

static avlnode* SingleRotateWithLeft(avlnode* father) {
	avlnode* son;
	son = father->left;
	father->left = son->right;
	son->right = father;

	father->height = max(height(father->left), height(father->right)) + 1;
	son->height = max(height(son->left), father->height) + 1;

	return son;
}

static avlnode* SingleRotateWithRight(avlnode* father) {
	avlnode* son;
	son = father->right;
	father->right = son->left;
	son->left = father;

	father->height = max(height(father->left), height(father->right)) + 1;
	son->height = max(height(son->right), father->height) + 1;

	return son;
}

static avlnode* DoubleRotateWithLeft(avlnode* grand) {
	grand->left = SingleRotateWithRight(grand->left);

	return SingleRotateWithLeft(grand);
}

static avlnode* DoubleRotateWithRight(avlnode* grand) {
	grand->right = SingleRotateWithLeft(grand->right);

	return SingleRotateWithRight(grand);
}

avlnode* insert(avlnode* root, long key, long data) {
	// if (root) {
	// 	printf("KEY %ld\n", root->key);
	// }
	// printf("\n");

	if (!root) {
		root = (avlnode*) calloc(1, sizeof(avlnode));
		root->key = key;
		root->data = data;
		root->height = 1;
		root->left = NULL;
		root->right = NULL;
	}

	if (key < root->key) {
		// printf("Im in left branch with ");
		root->left = insert(root->left, key, data);

		if (height(root->left) - height(root->right) >= 2) {
			if (key < root->left->key) {
				root = SingleRotateWithLeft(root);
			} else {
				root = DoubleRotateWithLeft(root);
			}
		}

	} else if (key > root->key) {
		// printf("Im in right branch with ");
		root->right = insert(root->right, key, data);

		if (height(root->right) - height(root->left) >= 2) {
			if (key > root->right->key) {
				root = SingleRotateWithRight(root);
			} else {
				root = DoubleRotateWithRight(root);
			}
		}

	} else {
		root->data = data;

		return root;
	}

	root->height = max(height(root->right), height(root->left)) + 1;

	// printing(root);
	// printf("\n\n\n");

	return root;
}

avlnode* search_min(avlnode* root) {
	if (!root) {
		return NULL;
	}

	avlnode* tmp = root;

	while (tmp->left) {
		tmp = tmp->left;
	}

	return tmp;
}

avlnode* delete_element(avlnode* root, long key) {
	// printf("KEY %ld\n", root->key);

	if (!root) {
		return NULL;
	}
	if (key > root->key) {
		// printf("Im in right branch with ");
		root->right = delete_element(root->right, key);
	} else if (key < root->key) {
		// printf("Im in left branch with ");
		root->left = delete_element(root->left, key);
	} else {
		if (key == root->key) {
			if (!root->right) {
				// printf("delete without right son\n");
				avlnode* tmp = root->left;
				free(root);
				return tmp;
			}/* else if (!root->left) {
				avlnode* tmp = root->right;
				free(root);
				return tmp;
			}*/ else {
				// printf("delete with right son\n");
				avlnode* new_chain = search_min(root->right);
				root->key = new_chain->key;
				root->data = new_chain->data;

				root->right = delete_element(root->right, new_chain->key);
			}
		}
	}
	if (!root) {
		// printf("BYEBYE\n");
		return root;
	}

	// if (root->key == 5) {
	// 	printf("spec_outp\n");
	// 	printing(root);
	// 	printf("spec_outp\n");
	// }


	// if (root->key == 5) {
	// 	printf("spec_outp\n");
	// 	printf("%d", root->height);
	// 	printf("spec_outp\n");
	// }

	if (height(root->right) - height(root->left) >= 2) {
		if (root->right && (key > root->right->key || !root->right->left) ) {
			root = SingleRotateWithRight(root);
		} else if (root->right) {
			root = DoubleRotateWithRight(root);
		}
	} else if (height(root->left) - height(root->right) >= 2) {
		if (root->left && (key < root->left->key || ! root->left->right)) {
			root = SingleRotateWithLeft(root);
		} else if (root->left) {
			root = DoubleRotateWithLeft(root);
		}
	}

	root->height = max(height(root->right), height(root->left)) + 1;

	// printing(root);
	// printf("\n\n\n");

	return root;
}

/*
a key data - добавить(обновить)
d key - удалить
s key - поиск
f -выход
*/

int main(void) {

	avlnode* root = NULL;
	char action;
	long key, data;

	while ((action = getchar()) != 'F') {
		if (action == 'A') {
			scanf("%ld%ld", &key, &data);
			root = insert(root, key, data);
			continue;
		}
		if (action == 'D') {
			scanf("%ld", &key);
			root = delete_element(root, key);
			continue;
		}
		if (action == 'S') {
			scanf("%ld", &key);
			find(root, key);
			continue;
		}
	}

	root = delete(root);

	return 0;
}
