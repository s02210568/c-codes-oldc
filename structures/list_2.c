#include <stdio.h>
#include <stdlib.h>

typedef struct list {
    int value;
    struct list* next;
} list;

void print_list(struct list* head, FILE* output) {
    while (head) {
        fprintf(output, "%d ", head->value);
        head = head->next;
    }
}

void push_front(struct list** head, int value) {
    struct list* new_node = (struct list*)malloc(sizeof(struct list));
    new_node->value = value;
    new_node->next = (*head);
    (*head) = new_node;
}

struct list* middle(struct list* head) {
    struct list* slow = head;
    struct list* fast = head->next;

    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }

    return slow;
}

struct list* merge(struct list* head_1, struct list* head_2) {
    struct list* new_head_cure = (list*)malloc(sizeof(list));
    new_head_cure->next = NULL;

    struct list* head_merged = new_head_cure;

    while (head_1 && head_2) {
        if (head_1->value < head_2->value) {
            new_head_cure->next = head_1;
            head_1 = head_1->next;
        }
        else {
            new_head_cure->next = head_2;
            head_2 = head_2->next;
        }
        new_head_cure = new_head_cure->next;
    }

    while (head_1) {
        new_head_cure->next = head_1;
        head_1 = head_1->next;
        new_head_cure = new_head_cure->next;
    }
    
    while (head_2) {
        new_head_cure->next = head_2;
        head_2 = head_2->next;
        new_head_cure = new_head_cure->next;
    }

    return head_merged->next;
}

struct list* merge_sort(struct list* head) {
    if (head == NULL || head->next == NULL) {
        return head;
    }

    struct list* left = head;
    struct list* right = middle(head);

    struct list* tmp = right->next;
    right->next = NULL;
    right = tmp;

    left = merge_sort(left);
    right = merge_sort(right);

    return merge(left, right);
}

int main(void) {
    char file_input_name[] = "input.txt";
    FILE* input = fopen(file_input_name, "r");

    struct list* head = (list*)malloc(sizeof(list));
    head->next = NULL;

    int value;
    fscanf(input, "%d", &value);

    push_front(&(head->next), value);

    while (fscanf(input, "%d", &value) != EOF) {
        push_front(&(head->next), value);
    }

    fclose(input);

    char file_output_name[] = "output.txt";
    FILE* output = fopen(file_output_name, "w");

    head = head->next;
    head = merge_sort(head);

    print_list(head, output);

    fclose(output);

    return 0;
}
