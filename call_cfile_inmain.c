#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

int
main(int argc, char** argv)
{
    //создать си файл также как тот питоновский запихнуть туда аргв[1] и сделать execlp только все это отдельный процесс
    char *path_to_dire = getenv("XDG_RUNTIME_DIR");
    
    if (!path_to_dire) {
        path_to_dire = getenv("TMPDIR");
        
        if (!path_to_dire) {
            path_to_dire = "/tmp";
        }
    }
    
    char *path_to_file = calloc(PATH_MAX, sizeof(*path_to_file));
    
    if (path_to_file == NULL) {
        return 1;
    }
    
    pid_t pid = getpid();
    snprintf(path_to_file, PATH_MAX, "%s/%d.c", path_to_dire, pid);
    
    int fd = open(path_to_file, O_CREAT | O_TRUNC | O_RDWR, 0600);
    
    if (fd < 0) {
        return 1;
    }
    
    char buf[] = "#include <stdio.h>\n"\
                "#include <unistd.h>\n"\
                "#define summon printf(\"summon\\n\")\n"\
                "#define disqualify printf(\"disqualify\\n\")\n"\
                "#define reject printf(\"reject\\n\")\n"\
                "int main(int argc, char** argv) {\n"\
                "int x;\n"\
            "while (scanf(\"%d\", &x) != EOF) {\n";
    
    dprintf(fd, "%s%s;\n}\nunlink(argv[0]);\n}\n", buf, argv[1]);
    char path_to_file_out[PATH_MAX];
    snprintf(path_to_file_out, PATH_MAX, "%s/%d.out", path_to_dire, pid);
    
    if (fork() == 0) {
        execlp("gcc", "gcc", path_to_file, "-o", path_to_file_out, NULL);
        _exit(1);
    }
    
    wait(NULL);
    
    if (close(fd) != 0) {
        return 1;
    }
    
    unlink(path_to_file);
    free(path_to_file);
    
    execl(path_to_file_out, path_to_file_out, NULL);
    _exit(1);
}
