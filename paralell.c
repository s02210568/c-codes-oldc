#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

int
function(char *file)
{
    pid_t pid = fork();
    
    if (pid == 0) {
        execlp(file, file, NULL);
        _exit(1);
    } else if (pid < 0) {
        _exit(1);
    }
    
    int status;
    wait(&status);
            
    if ((!WIFEXITED(status) || WEXITSTATUS(status))) {
        return 0;
    } else {
        return 1;
    }
}

int
main(int argc, char** argv)
{
    int pip[2];
    pipe(pip);
    
    if (fork() == 0) {
        close(pip[0]);
        
        int fd = open(argv[2], O_RDWR);
        
        if (fd < 0) {
            _exit(1);
        }
        
        dup2(fd, 0);
        
        if (close(fd) != 0) {
            _exit(1);
        }
        
        dup2(pip[1], 1);
        close(pip[1]);
        
        execlp(argv[1], argv[1], NULL);
        _exit(1);
    }
    
    if (fork() == 0) {
        close(pip[1]);
        dup2(pip[0], 0);
        close(pip[0]);
        
        execlp(argv[3], argv[3], NULL);
        _exit(1);
    }
    
    int status;
    wait(&status);
    
    if (!WIFEXITED(status) || WEXITSTATUS(status)) {
        return 0;
    }
    
    wait(&status);
    
    if (!WIFEXITED(status) || WEXITSTATUS(status)) {
        return 0;
    }
    
    if (fork() == 0) {
        execlp(argv[4], argv[4], NULL);
        _exit(1);
    }

    wait(&status);
    
    if (!WIFEXITED(status) || WEXITSTATUS(status)) {
        return 0;
    }
    
    return 1;
}
