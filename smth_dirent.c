#include <fcntl.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <dirent.h>
#include <stdio.h>

ssize_t
getcwd2(int fd, char *buf, size_t size)
{
    char* answer = calloc(PATH_MAX, sizeof(*answer));
    
    if (answer == NULL) {
        return -1;
    }
    
    DIR *start_dir = opendir(".");
    
    if (start_dir == NULL) {
        return -1;
    }
    
    int fd_start = dirfd(start_dir);
    
    if (fd_start < 0) {
        return -1;
    }
    
    if (fchdir(fd) != 0) {
        return -1;
    }
    
    struct stat root;
    
    if (lstat("/", &root) < 0) {
        return -1;
    }
    
    struct stat cure;
    
    if (lstat(".", &cure) < 0) {
        return -1;
    }
    
    if (chdir("..") != 0) {
        return -1;
    }
    
    while (root.st_ino != cure.st_ino || root.st_dev != cure.st_dev) {
        DIR *d = opendir(".");
        
        if (d == NULL) {
            return -1;
        }
        
        struct dirent *dd;

        while ((dd = readdir(d)) != NULL) {
            char path[PATH_MAX];
            snprintf(path, PATH_MAX, "./%s", dd->d_name);
            
            struct stat buf;
            
            if (lstat(path, &buf) < 0) {
                continue;
            }
            
            if (cure.st_ino == buf.st_ino && buf.st_dev == cure.st_dev) {
                char *cure_answer = calloc(PATH_MAX, sizeof(*cure_answer));
                
                if (cure_answer == NULL) {
                    return -1;
                }
                
                snprintf(cure_answer, PATH_MAX, "%s/%s", dd->d_name, answer);
                
                free(answer);
                
                answer = cure_answer;
                
                if (lstat(".", &cure) < 0) {
                    return -1;
                }
                
                break;
            }
        }
        
        if (closedir(d) != 0) {
            return -1;
        }
        
        if (chdir("..") != 0) {
            return -1;
        }
    }
    
    int length = strlen(answer);
    
    if (length > 0) {
        answer[length - 1] = '\0';
    }
    
    if (size > 0) {
        snprintf(buf, size, "/%s", answer);
    }
    
    if (length == 0) {
        length++;
    }
    
    free(answer);
    
    if (fchdir(fd_start) != 0) {
        return -1;
    }
    
    if (closedir(start_dir) != 0) {
        return -1;
    }
    
    return length;
}
