#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

int
main(int argc, char** argv)
{
    unsigned int len = argc + 1;
    
    for (int i = 1; i < argc; i++) {
        len += strlen(argv[i]);
    }
    
    char *buf = calloc(len, sizeof(*buf));
    
    if (buf == NULL) {
        return 1;
    }
    
    unsigned int shift = 1;
    buf[0] = '1';
    
    for (int i = 1; i < argc; i++) {
        snprintf(buf + shift, strlen(argv[i]) + 2, "*%s", argv[i]);
        shift += strlen(argv[i]) + 1;
    }
    
    pid_t pid = getpid();
    
    char *path_to_dire = getenv("XDG_RUNTIME_DIR");
    
    if (!path_to_dire) {
        path_to_dire = getenv("TMPDIR");
        
        if (!path_to_dire) {
            path_to_dire = "/tmp";
        }
    }
    
    char *path_to_file = calloc(PATH_MAX, sizeof(*path_to_file));
    
    if (path_to_file == NULL) {
        return 1;
    }
    
    snprintf(path_to_file, PATH_MAX, "%s/%d", path_to_dire, pid);
    
    int fd = open(path_to_file, O_CREAT | O_TRUNC | O_RDWR, 0700);
    
    if (fd < 0) {
        return 1;
    }
    
    dprintf(fd, "#!/usr/bin/python3\nimport os\nimport sys\n"\
            "sys.set_int_max_str_digits(10**6)\nprint(%s)\nos.remove('%s')\n", buf, path_to_file);
    
    free(buf);
    
    if (close(fd) != 0) {
        return 1;
    }
    
    execl(path_to_file, path_to_file, NULL);
    _exit(1);
}
