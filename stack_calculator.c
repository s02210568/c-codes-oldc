#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define NUM_LEN 10
#define SIGN_LEN 6
#define MAX_LEN 10000000

typedef struct stack_int {
    int sp; //вершина текущая
    int sz; //размер
    long long* stack;
} stack_int;

void push_int(stack_int* stack_numbers, long long c) {
    if (stack_numbers->sz == stack_numbers->sp + 1) {
        stack_numbers->sz = 2 * stack_numbers->sz + 1;
        stack_numbers->stack = (long long*)realloc(stack_numbers->stack, stack_numbers->sz * sizeof(long long));
    }

    stack_numbers->stack[++stack_numbers->sp] = c;
}

void pop_int(stack_int* stack_numbers) {
    --(stack_numbers->sp);
}

typedef struct stack_char {
    int sp; //вершина текущая
    int sz; //размер
    char* stack;
} stack_char;

void push_char(stack_char* stack_operations, char c) {
    if (stack_operations->sz == stack_operations->sp + 1) {
        stack_operations->sz = 2 * stack_operations->sz + 1;
        stack_operations->stack = (char*)realloc(stack_operations->stack, stack_operations->sz * sizeof(char));
    }

    stack_operations->stack[++stack_operations->sp] = c;
}

void pop_char(stack_char* stack_operations) {
    --(stack_operations->sp);
}

int is_digit(char c) {
    for (int i = 0; i < NUM_LEN; i++) {
        if (c - '0' == i) {
            return 1;
        }
    }

    return 0;
}

int is_sign(char c) {
    char signs[SIGN_LEN] = { '+', '-', '*', '/', ')', '(' };

    for (int i = 0; i < SIGN_LEN; i++) {
        if (c == signs[i]) {
            return 1;
        }
    }

    return 0;
}

int priority(char c) {
    if (c == '*' || c == '/') {
        return 2;
    }
    else {
        return 1;
    }
}

long long result(long long num_1, long long num_2, char sign) {
    if (sign == '*') {
        return num_1 * num_2;
    }
    else if (sign == '/') {
        return num_1 / num_2;
    }
    else if (sign == '+') {
        return num_1 + num_2;
    }

    else if (sign == '-') {
        return num_1 - num_2;
    }

    return -1;
}

int main(void) {
    FILE* input = fopen("input.txt", "r");

    char str[MAX_LEN];
    fgets(str, MAX_LEN, input);

    if (strlen(str) == 0) {
        return 0;
    }

    stack_char* stack_operations = (stack_char*)malloc(sizeof(stack_char));
    stack_int* stack_numbers = (stack_int*)malloc(sizeof(stack_int));

    stack_operations->sp = -1;
    stack_operations->sz = 100;
    stack_operations->stack = (char*)calloc(100, sizeof(char));

    stack_numbers->sp = -1;
    stack_numbers->sz = 100;
    stack_numbers->stack = (long long*)calloc(100, sizeof(long long));

    int i = 0;
    char c = str[i];

    while (c != EOF && c != '\n' && c != '\0') {
        if (is_sign(c)) {
            if (stack_operations->sp == -1) {
                push_char(stack_operations, c);

                i++;
                c = str[i];
                continue;
            }

            if (c == ')') {
                while (stack_operations->stack[stack_operations->sp] != '(') {
                    long long num_1 = stack_numbers->stack[stack_numbers->sp];
                    pop_int(stack_numbers);
                    long long num_2 = stack_numbers->stack[stack_numbers->sp];
                    pop_int(stack_numbers);

                    push_int(stack_numbers, result(num_2, num_1, stack_operations->stack[stack_operations->sp]));
                    pop_char(stack_operations);
                }

                pop_char(stack_operations);

                i++;
                c = str[i];
                continue;
            }

            if (c == '(') {
                push_char(stack_operations, c);

                i++;
                c = str[i];
                continue;
            }

            if (stack_operations->stack[stack_operations->sp] == '(') {
                push_char(stack_operations, c);

                i++;
                c = str[i];
                continue;
            }

            if (priority(stack_operations->stack[stack_operations->sp]) < priority(c)) {
                push_char(stack_operations, c);
            }
            else if (priority(stack_operations->stack[stack_operations->sp]) > priority(c)) {
                while (stack_operations->sp != -1 && stack_operations->stack[stack_operations->sp] != '(') {
                    long long num_1 = stack_numbers->stack[stack_numbers->sp];
                    pop_int(stack_numbers);
                    long long num_2 = stack_numbers->stack[stack_numbers->sp];
                    pop_int(stack_numbers);

                    push_int(stack_numbers, result(num_2, num_1, stack_operations->stack[stack_operations->sp]));
                    pop_char(stack_operations);
                }

                push_char(stack_operations, c);
            }
            else if (priority(stack_operations->stack[stack_operations->sp]) == priority(c)) {
                long long num_1 = stack_numbers->stack[stack_numbers->sp];
                pop_int(stack_numbers);
                long long num_2 = stack_numbers->stack[stack_numbers->sp];
                pop_int(stack_numbers);

                push_int(stack_numbers, result(num_2, num_1, stack_operations->stack[stack_operations->sp]));
                pop_char(stack_operations);
                push_char(stack_operations, c);
            }
        }

        if (is_digit(c)) {
            long long num = c - '0';
            i++;
            c = str[i];

            while (is_digit(c)) {
                num = num * 10 + (c - '0');
                i++;
                c = str[i];
            }

            push_int(stack_numbers, num);

            while (c == ' ') {
                i++;
                c = str[i];
            }

            continue;
        }

        i++;
        c = str[i];
    }

    fclose(input);

    while (stack_operations->sp != -1) {
        long long num_1 = stack_numbers->stack[stack_numbers->sp];
        pop_int(stack_numbers);
        long long num_2 = stack_numbers->stack[stack_numbers->sp];
        pop_int(stack_numbers);

        push_int(stack_numbers, result(num_2, num_1, stack_operations->stack[stack_operations->sp]));
        pop_char(stack_operations);
    }

    FILE* output = fopen("output.txt", "w");

    fprintf(output, "%lld", stack_numbers->stack[stack_numbers->sp]);

    fclose(output);

    return 0;
}
